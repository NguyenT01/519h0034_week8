import 'package:flutter/material.dart';
import '../validator/login_validator.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  static const route = '/login';

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with LoginValidator {
  final formkey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Container(
            padding: EdgeInsets.all(40),
            child: Column(
              children: [
                const SizedBox(
                  height: 100,
                ),
                Text(
                  'LOGIN',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 48,
                      color: Colors.teal),
                ),
                const SizedBox(
                  height: 16,
                ),
                buildLoginForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildLoginForm() {
    return Form(
      key: formkey,
      child: Column(
        children: [
          emailField(),
          const SizedBox(
            height: 16,
          ),
          passwordFiled(),
          const SizedBox(
            height: 25,
          ),
          loginButton(),
        ],
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      controller: emailController,
      decoration: InputDecoration(
          icon: Icon(Icons.account_circle),
          labelText: 'Email',
          hintText: 'Please enter your email',
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(16),
              borderSide: BorderSide())),
      validator: validateEmail,
    );
  }

  Widget passwordFiled() {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password',
          hintText: 'Enter your password',
          border: OutlineInputBorder(
            borderSide: BorderSide(),
            borderRadius: BorderRadius.circular(16),
          )),
      validator: validatePassword,
    );
  }

  Widget loginButton() {
    return SizedBox(
      width: double.infinity,
      height: 48,
      child: ElevatedButton(
        child: Text(
          'LOGIN',
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
        onPressed: validate,
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16))),
      ),
    );
  }

  void validate() {
    final form = formkey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final email = emailController.text;
      final password = passwordController.text;


      Navigator.pushReplacementNamed(context, '/home', arguments: email);
    }
  }
}
