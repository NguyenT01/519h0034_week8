import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:provider/provider.dart';
import 'dart:async';
import 'dart:convert';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/');
            },
            icon: Icon(Icons.exit_to_app),
            color: Colors.white,
          )
        ],
        leading: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, '/cart');
          },
          icon: Icon(Icons.shopping_cart),
          color: Colors.white,
        ),
        title: Text('Lazada'),
      ),
      body: SafeArea(
        child: FutureBuilder<dynamic>(
          future: http.get(Uri.parse('https://fakestoreapi.com/products')),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              var data = json.decode(snapshot.data.body);
              //print(data[0]['image']);
              //return Text('aa');
              return GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: 12,
                      crossAxisSpacing: 12),
                  itemCount: data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return productCard(
                        data[index]['image'],
                        data[index]['title'].length > 50
                            ? data[index]['title'].substring(0, 35)
                            : data[index]['title'],
                        data[index]['price'].toString(),
                        data[index]['id'].toString());
                  });
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  const SizedBox(height: 16),
                  Text('Đang tải dữ liệu, vui lòng chờ'),
                ],
              ));
            } else {
              return Center(child: Text('Đã có lỗi xảy ra, vui lòng thử lại'));
            }
          },
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.house), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Search'),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart), label: 'Cart'),
          BottomNavigationBarItem(
              icon: Icon(Icons.category), label: 'Category'),
        ],
        currentIndex: _currentIndex,
        selectedItemColor: Colors.teal,
        unselectedItemColor: Colors.grey,
        onTap: onItemTapped,
      ),
    );
  }

  Widget productCard(var imgUrl, var textTitle, var textPrice, var id) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/p_detail', arguments: [id, textTitle]);
      },
      child: Card(
        elevation: 4.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.network(
                  imgUrl,
                  fit: BoxFit.fill,
                  height: 80,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                textTitle,
                style: TextStyle(
                  fontSize: 12,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                '\$ ' + textPrice,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    color: Colors.red),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onItemTapped(int index) {
    switch (index) {
      case 1:
        Navigator.pushNamed(context, '/search');
        break;
      case 2:
        Navigator.pushNamed(context, '/cart');
        break;
      case 3:
        Navigator.pushNamed(context, '/category');
        break;
    }
  }
}
